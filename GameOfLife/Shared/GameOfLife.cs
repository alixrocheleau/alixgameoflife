﻿namespace GameOfLife.Shared
{
    public class GameOfLife
    {
        public List<int> Output { get; private set; }
        public List<int> State { get; private set; }
        public int Width { get; private set; } = 64;
        public int Height { get; private set; } = 64;

        public GameOfLife(int width = 64, int height = 64)
        {
            Width = width;
            Height = height;
            Output = new List<int>(new int[width * height]);
            State = new List<int>(new int[width * height]);
        }

        public void Set(int x, int y, string s)
        {
            int p = 0;
            foreach (var c in s)
            {
                if (x + p >= 0 && x + p < Width && y >= 0 && y < Height)
                {
                    State[y * Width + x + p] = c == '1' ? 1 : 0;
                }
                p++;
            }
        }

        public int GetCell(int x, int y)
        {
            if (x >= 0 && x < Width && y >= 0 && y < Height)
            {
                return Output[y * Width + x];
            }
            return 0;
        }

        public void Update()
        {
            for (int i = 0; i < Width * Height; i++)
            {
                Output[i] = State[i];
            }

            for (int i = 0; i < Width; i++)
            {
                for (int j = 0; j < Height; j++)
                {
                    int connected = GetCell(i - 1, j - 1) + GetCell(i, j - 1) + GetCell(i + 1, j - 1) +
                                    GetCell(i - 1, j) + GetCell(i + 1, j) +
                                    GetCell(i - 1, j + 1) + GetCell(i, j + 1) + GetCell(i + 1, j + 1);

                    if (GetCell(i, j) == 1)
                    {
                        State[j * Width + i] = (connected == 2 || connected == 3) ? 1 : 0;
                    }
                    else
                    {
                        State[j * Width + i] = (connected == 3) ? 1 : 0;
                    }
                }
            }
        }

        public void Reset()
        {
            Output = new List<int>(new int[Width * Height]);
            State = new List<int>(new int[Width * Height]);
        }
    }
}
