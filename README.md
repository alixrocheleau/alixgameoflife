# game of life

## Getting started

Clone the repo, run in visual studio.

## How it works

Press a button to start a pattern and watch the game play itself.

## What is Conway's Game of Life?

https://en.wikipedia.org/wiki/Conway%27s_Game_of_Lifem

The Game of Life, also known simply as Life, is a cellular automaton devised by the British mathematician John Horton Conway in 1970.[1] It is a zero-player game,[2][3] meaning that its evolution is determined by its initial state, requiring no further input. One interacts with the Game of Life by creating an initial configuration and observing how it evolves. It is Turing complete and can simulate a universal constructor or any other Turing machine.

## Images

![img1](/images/1234.png)
![img2](/images/loop.png)
![img3](/images/pento.png)
![img4](/images/random.png)
![img5](/images/random2.png)
![img6](/images/gol.png)